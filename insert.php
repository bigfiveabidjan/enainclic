<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

include("connexion.php");
if(isset($_POST))
{    
     $categorie = $_POST['categori'];
     $sous_categorie = $_POST['sous_categori'];
     $nom = $_POST['nom'];
     $prenom = $_POST['prenom'];
     $entreprise = $_POST['entreprise'];
     $siret = $_POST['siret'];
     $telephone = $_POST['telephone'];
     $email = $_POST['email'];
     
     
     $ajoutduclient = $db->prepare('INSERT INTO client(CATEGORIES,SOUS_CATEGORIES,NOM,PRENOM,ORGANISATION,NSIRET,TELEPHONE,EMAIL) VALUES(:categorie,:sous_categorie,:nom,:prenom,:entreprise,:siret,:telephone,:email)');
	$ajoutduclient->execute(array(
		'categorie'=>$categorie,
		'sous_categorie'=>$sous_categorie,
		'nom'=>$nom,
		'prenom'=>$prenom,
		'entreprise'=>$entreprise,
		'siret'=>$siret,
		'telephone'=>$telephone,
		'email'=>$email
	));
     
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Formulaire ''En Ain Clic'' - Remerciement</title>
        <!-- Bootstrap core CSS -->
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="style.css" rel="stylesheet">
    </head>
    <body>
        <div class="container corpsPrincipal remerciement">
            <div class="container h-100">
                <div class="row align-items-center h-100 corps">
                    <div class="col-12 page-1">
                        <img src="LogoENAINCLICjaune@1x.svg">
                        <h2>
                            TOUT L’AIN </h2>
                        <h3>
                            DANS MA POCHE </h3>
                        <h4>
                            NOUS VOUS REMERÇIONS DE VOTRE PARTICIPATION <br><br>
                            À TRÈS VITE SUR EN AIN CLIC ! </h4>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript
    ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <footer>
            <div class="row">
                <div class="col-md-6 footDivGauche">
                    <img src="en-ain-clic-tap-bar-icon.svg">
                    <p><a href="#">MENTIONS LÉGALES</a>  |  <a href="#">GESTION DES COOKIES</a></p> 
                </div>
                <div class="col-md-6 footDivDroit">
                    <img src="logo-aintourisme.svg"> 
                </div>
            </div>
        </footer>
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/code.js"></script>
    </body>
</html>
