jQuery(document).ready(function($){
  $("#repondre").click(function(){
    $(".page-1").hide();
	  $(".page-2").show();
  });
  $("#reprendre").click(function(){
    $(".page-3").hide();
	  $(".page-2").show();
  });
  $("#non").click(function(){
    $(".page-2").hide();
	  $(".page-3").show();
  });
  $("#oui").click(function(){
    $(".page-2").hide();
    $(".page-4").show();
    $(".precedent2").hide();
    $(".precedent3").hide();

    $("#sortir-show").hide();
    $("#tourisme-show").hide();
    $("#vie-locale-show").hide();
    $("#bons-plans-show").hide();
  });
  $("#precedent1").click(function(){
    $(".page-4").hide();
	  $(".page-2").show();
  });
  $(".precedent2").click(function(){
    $(".page-4").show();
    $(".page-2").hide();
    $("#categorie").show();
    $("#sortir-show").hide();
    $("#formulaire-show").hide();
    $("#commerce-show").hide();
    $("#tourisme-show").hide();
    $("#vie-locale-show").hide();
    $("#bons-plans-show").hide();
    $(".precedent1").show();
    $(".precedent2").hide();
    $(".precedent3").hide();
    $('input[name="sous_categorie"]').prop('checked', false);
  });
  $("#commerce").click(function(){
    $("#categori").val("Commerces");

    $("#categorie").hide();
    $("#formulaire-show").hide();
    $(".precedent1").hide();
    $(".precedent2").show();
    $(".precedent3").hide();

    $("#commerce-show").show();
    $("#sortir-show").hide();
    $("#tourisme-show").hide();
    $("#vie-locale-show").hide();
    $("#bons-plans-show").hide();
  });
  $("#sortir").click(function(){
    $("#categori").val("Sortir");

    $("#categorie").hide();
    $("#formulaire-show").hide();
    $(".precedent1").hide();
    $(".precedent2").show();
    $(".precedent3").hide();

    $("#commerce-show").hide();
    $("#sortir-show").show();
    $("#tourisme-show").hide();
    $("#vie-locale-show").hide();
    $("#bons-plans-show").hide();
  });

  $("#tourisme").click(function(){
    $("#categori").val("Tourisme");

    $("#categorie").hide();
    $("#formulaire-show").hide();
    $(".precedent1").hide();
    $(".precedent2").show();
    $(".precedent3").hide();

    $("#commerce-show").hide();
    $("#sortir-show").hide();
    $("#tourisme-show").show();
    $("#vie-locale-show").hide();
    $("#bons-plans-show").hide();
  });

  $("#vie-locale").click(function(){
    $("#categori").val("Vie locale");

    $("#categorie").hide();
    $("#formulaire-show").hide();
    $(".precedent1").hide();
    $(".precedent2").show();
    $(".precedent3").hide();

    $("#commerce-show").hide();
    $("#sortir-show").hide();
    $("#tourisme-show").hide();
    $("#vie-locale-show").show();
    $("#bons-plans-show").hide();
  });

  $("#bons-plans").click(function(){
    $("#categori").val("Bons plans");

    $("#categorie").hide();
    $("#formulaire-show").hide();
    $(".precedent1").hide();
    $(".precedent2").show();
    $(".precedent3").hide();

    $("#commerce-show").hide();
    $("#sortir-show").hide();
    $("#tourisme-show").hide();
    $("#vie-locale-show").hide();
    $("#bons-plans-show").show();
  });

  $(".valider-reponse").click(function(){


    if (!$("input[name='sous_categorie']:checked").val()) {
        alert('Aucune sous-catégorie selectionné');
        return false;
    }
      else {
        $("#categorie").hide();

        $("#commerce-show").hide();
        $("#sortir-show").hide();
        $("#tourisme-show").hide();
        $("#vie-locale-show").hide();
        $("#bons-plans-show").hide();

        $("#formulaire-show").show();
        $(".precedent1").hide();
        $(".precedent2").show();
        $(".precedent3").hide();

        var selectedVal = "";
        var selected = $("input[type='radio'][name='sous_categorie']:checked");
        if (selected.length > 0) {
            selectedVal = selected.val();
            $("#sous_categori").val(selectedVal);
        }
        
    }

  });
  

  

});